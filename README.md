# EvoLoad
A basic Garry's Mod HTML loading screen. Made by request.
## Change the backgrounds
Put the backgrounds you want in the `backgrounds` folder, then add their paths to the array at the top of `evoload.js`.
If you delete the backgrounds it comes with, remove them from the array.

It should look like this (pay attention to the commas):
```js
const images = [
	"https://example.com/image4.jpg",
	"backgrounds/image1.jpg",
	"backgrounds/image2.jpg",
	"backgrounds/image3.jpg"
];
```
As shown in the above example, you can also use hotlinks to online resources.
## Change the server owner
Change the name from "garry" to whatever name you want on line 22 of `index.html`, like this: `<span>YOUR NAME HERE</span>`.

Change the profile picture by putting the link in the `src` attribute of the `img` on line 20, like this: `<img id="pfp" src="YOUR LINK HERE">`.
## Other
If you're familiar with HTML, CSS, and JS, go ahead and make any changes you want.
Keep in mind that maintaining compatibility with the old version of Awesomium in Garry's Mod can be difficult.
