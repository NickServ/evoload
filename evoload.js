const images = [
	"backgrounds/20191008180517_1.jpg",
	"backgrounds/20191008180729_1.jpg",
	"backgrounds/20191008180752_1.jpg",
	"backgrounds/20191008180829_1.jpg",
	"backgrounds/20191008181018_1.jpg"
];
var cycle = 0;
var cycle_max = images.length;
var cycle_flip = false;
window.onload = function() {
	const bg_a = document.getElementById("bg_a");
	const bg_b = document.getElementById("bg_b");
	// GMod does not support template literals :(
	bg_a.setAttribute("style", "background-image: url("+images[cycle]+")");
	bg_b.setAttribute("style", "background-image: url("+images[cycle]+")");
	window.setInterval(function() {
		cycle_flip = !cycle_flip;
		bg_a.classList.toggle("invisible");
		if (cycle_flip === true) {
			old = images[cycle-1] || images[cycle_max-1];
			bg_b.setAttribute("style", "background-image: url("+old+")");
		} else {
			bg_a.setAttribute("style", "background-image: url("+images[cycle]+")");
		};
		cycle++;
		if (cycle >= cycle_max) {
			cycle = 0
		}
	}, 2500);
	
	const percentage = document.getElementById("percentage");
	var total_files;
	var files_remaining;
	function updateRemaining() {
		if (total_files && files_remaining) {
			percentage.textContent = Math.round( ((total_files-files_remaining)/total_files) * 100 ) + "%"
		}
	};
	
	//GameDetails = function(name, url, map, maxplayers, steamid, gamemode) {};
	SetFilesTotal = function(num) {
		total_files = num;
		updateRemaining()
	};
	SetFilesNeeded = function(num) {
		files_remaining = num;
		updateRemaining()
	}
	//DownloadingFile = function(filename) {};
	
	const status = document.getElementById("status");
	SetStatusChanged = function(str) {
		status.textContent = str
	};
}
